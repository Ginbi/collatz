const inputForm = document.querySelector("#input-form");
const numberInput = document.querySelector("#number-input");
const wrongNumberMsg = document.querySelector(".wrong-number-msg");
const resultsDisplay = document.querySelector(".results-display");
const highestDisplay = document.querySelector("#highest-display");
const stepsDisplay = document.querySelector("#steps-display");
const sequenceDisplay = document.querySelector("#sequence-display");



let totalSteps = 0;
let allSteps = [];

function showAllSteps(allSteps) {

    allSteps.push(1);

    for(step of allSteps) {
        const stepNumber = document.createElement("li");
        stepNumber.textContent = step;
        sequenceDisplay.appendChild(stepNumber);
    }
    
    
    
}

function collatz() {
    sequenceDisplay.textContent = '';
    let currentNumber = parseInt(numberInput.value);
    let highestNumber = currentNumber;

    if(currentNumber < 1 || !currentNumber){
        wrongNumberMsg.style.display = "block";
        resultsDisplay.style.display = "none";
    } else {
        wrongNumberMsg.style.display = "none";
        do {
            allSteps.push(currentNumber);

            switch (currentNumber % 2) {
                case 0:
                    currentNumber /= 2;
                    break;
                case 1:
                    currentNumber = (currentNumber * 3) + 1;
                    break;
            }

            if(currentNumber > highestNumber){
                highestNumber = currentNumber;
            }

            totalSteps++;
        } while(currentNumber !== 1)

        resultsDisplay.style.display = "block";

        highestDisplay.textContent = highestNumber;
        stepsDisplay.textContent = totalSteps;
        showAllSteps(allSteps);
        console.log(allSteps);
        totalSteps = 0;
        allSteps = [];

    }
}

inputForm.addEventListener("submit", function(e){
    e.preventDefault();
    collatz();
});